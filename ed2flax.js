#!/usr/bin/env node

const jsdom = require("jsdom");
const fs = require("fs");
const arg = require("arg");
const inquirer = require("inquirer");
const http = require("http");
const https = require("https");
const { JSDOM } = jsdom;
const TurndownService = require("turndown")
const turndownService = new TurndownService()


// to do: transform “”
let foldername = "outputs";

// create folder for all files
try {
  // first check if directory already exists
  if (!fs.existsSync(foldername)) {
    fs.mkdirSync(foldername);
  }
} catch (err) {
  console.log(`can’t create the output folder: ${foldername}, that’s curious`);
}

cli(process.args);

// get the file name from the CLI

function cli(args) {
  let options = parsedArgToOptions(args);
  console.log(options);

  fs.readFile(__dirname + "/" + options.filename, "utf8", function (err, html) {
    if (err) {
      console.warn(err);
    } else {
      createDataFromHTML(html);
    }
  });
}

// the function that does everything!

function createDataFromHTML(content) {
  const { document } = new JSDOM(content).window;
  const body = document.body;

  updateHTML(body);

  body.querySelectorAll("section").forEach((section, index) => {
    if (section.classList.contains("toc")) {
      console.log("class", section.className);
      return;
    }

    const frontmatter = {};
    // get the title for the frontmatter
    frontmatter.title = section.querySelector("h1")?.innerHTML.trim()
      ? section.querySelector("h1")?.innerHTML.trim()
      : `no-title${index}`;
    frontmatter.class = section.className;

    if (section.querySelector("author")) {
      console.log("1");
      frontmatter.author = section.querySelector(".author").trim();
      section.querySelector("author").remove();
    }

    //remove the title
    section.querySelector("h1.component-title")?.remove();
    section.querySelector("header")?.remove();
    let meta = ``;

    Object.keys(frontmatter).forEach(function (key) {
      meta = meta + `\n${key}: "${frontmatter[key]}"`;
    });

    // fix some html troubles
    const updatedHTML = replacePlaceholders(section.innerHTML);


    // do some search and replace for all the `{{ xxx }}` to be put in a `{{"{{ xxx }}"}}`

    // let convert that to markdown on the fly
    const md = turndownService.turndown(updatedHTML)

    // change image url

    fs.writeFile(
      `./${foldername}/chap-${index}.md`,
      `---${meta}\norder: ${index}\n---\n\n${md}`,
      function () {
        console.log(`chap-${index} written!`);
      },
    );
  });
}

// get the options from the args on the CLI
function parsedArgToOptions(rawArgs) {
  const args = arg({
    "--filename": String,
  });
  return { filename: args["--filename"] };
}

// this is the function you want to update!
//
function updateHTML(body) {
  //remove running heads
  body.querySelectorAll(".running-left, .running-right").forEach((el) => {
    el.remove();
  });
  //relink images and download’em
  body.querySelectorAll("img").forEach((img) => {
    imageLink = img.src.split("/");
    img.src = `/images/${imageLink[imageLink.length - 1]}`;
  });

  //remove restart numberung
  body.querySelectorAll(".restart-numbering").forEach((el) => el.remove());

  //remove empty p
  body.querySelectorAll("p").forEach((el) => {
    if (el.innerHTML == "") {
      el.remove();
    }
  });

  body
    .querySelectorAll(".paragraph")
    .forEach((el) => el.classList.remove("paragraph"));

  return body;
}


// replace placeholder
function replacePlaceholders(input) {
    return input.replace(/({{*)\s*(.*?)\s*(}}*)/g, '{{ "$1 $2 $3" }}');
}
